package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/labstack/gommon/log"

	"github.com/ChimeraCoder/anaconda"
	"gitlab.com/k-terashima/utils"
)

// Load settingfile.toml
type Config struct {
	// Loop timespan
	Timespan time.Duration `toml:"timespan"`

	// Twitter API
	TwitterAccessToken    string `toml:"twitter_access_token"`
	TwitterAccessSecret   string `toml:"twitter_access_secret"`
	TwitterConsumerKey    string `toml:"twitter_consumer_key"`
	TwitterConsumerSecret string `toml:"twitter_consumer_secret"`

	// Discord setting
	ID        string `toml:"id"`
	Token     string `toml:"token"`
	ChannelID string `toml:"channel_id"`
	Name      string `toml:"name"`

	// Search word
	Search string
}

var (
	c = new(Config)
	// 過去投稿ID
	ids []int64
	// 現在の投稿配列
	str []string
)

func init() {
	f, err := os.OpenFile("server.log", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}
	log.SetOutput(f)

	// loads config
	meta, err := toml.DecodeFile("./config.toml", c)
	if err != nil {
		log.Fatal("getting error toml decode of setting file : ", err, meta)
	}
}

func main() {
	discord := c.newDiscord()
	defer func() {
		if err := recover(); err != nil {
			s := "Search bot was recovering."
			m := utils.Message(s)
			discord.Set(m)
			if err := discord.Send(); err != nil {
				log.Errorf("%s: %s", err, m)
				os.Exit(1)
			}
		}
	}()

	var (
		hour = time.NewTicker(c.Timespan * time.Minute)
	)

	// Setting Twitter API
	api := anaconda.NewTwitterApiWithCredentials(c.TwitterAccessToken, c.TwitterAccessSecret, c.TwitterConsumerKey, c.TwitterConsumerSecret)

	for {
		select {
		case <-hour.C:
			func() {
				setHour := []int{7, 16}
				var gosign bool
				t := time.Now()
				for _, v := range setHour {
					if v == t.Hour() {
						gosign = true
					}
				}

				if gosign != true {
					return
				}

				// Sets search word
				searchResult, _ := api.GetSearch(c.Search, nil)
				l := len(searchResult.Statuses)

				if l != 0 {
					for _, tw := range searchResult.Statuses {
						var (
							// check the except tweet.
							check1 = strings.Contains(tw.User.Name, "数式")
							check2 = strings.Contains(tw.User.Name, "プププ プリキュア")
							check3 = strings.Contains(tw.User.Name, "bot")
							// check2 = strings.Contains(tw.User.Location, "")
							// check3 = strings.Contains(tw.Text, "")
						)

						if !check1 &&
							!check2 &&
							!check3 {
							var (
								level   string
								usr     = tw.User.Name
								url     = tw.User.ScreenName
								t, _    = tw.CreatedAtTime()
								jst     = time.FixedZone("Asia/Tokyo", 9*60*60)
								nowJST  = t.In(jst)
								y, m, d = nowJST.Date()
								date    = fmt.Sprintf("%d年%d月%d日 %d:%d:%d", y, m, d, nowJST.Hour(), nowJST.Minute(), nowJST.Second())
							)

							// 被り判定
							var b bool
							if len(ids) != 0 {
								for _, v := range ids {
									if v == tw.Id {
										// fmt.Printf("%d == %d\n", tw.Id, v)
										b = true
									}
								}
							}

							// 被り判定がTrueなときは配列に追加しない
							if b != true {
								bo := strings.Contains(tw.FullText, "https://t.co")
								// 関連性レベル
								if bo != false {
									level = "★★★★★"
								} else {
									level = "★☆☆☆☆"
								}

								// Message整形
								s := fmt.Sprintf("%s\n%s\n[%s](https://twitter.com/%s)さんのTweet\n%s\n\n", level, date, usr, url, tw.FullText)

								// 変数に代入
								ids = append(ids, tw.Id) // 過去投稿ID
								str = append(str, s)     // 投稿予定の配列
							}

						}
					}

					// fmt.Println(len(IDs), str)

					m := utils.Message(strings.Join(str, ""))
					discord.Set(m)
					if err := discord.Send(); err != nil {
						log.Errorf("%s: %s", err, m)
					}

					// 保持Tweet dataの初期化
					str = []string{}
				}

				// 被りチェック用データを減らす
				n := len(ids) - 1
				if 100 < n {
					ids = ids[80:n]
				}
			}()
		}
	}
}

func (c *Config) newDiscord() *utils.NotifyDiscord {
	return &utils.NotifyDiscord{
		ID:        c.ID,
		Token:     c.Token,
		ChannelID: c.ChannelID,
		Name:      c.Name,
	}
}
